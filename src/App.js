import logo from "./logo.svg";
import "./App.css";
import Liste from "./Liste";
import React from "react";

import data from "./fakedata/data.json";

console.log(data);
console.log(data.Global.NewDeaths);

function App() {
  const [catfact, setCatFact] = React.useState({
    fact: "Chargement en cours ...",
  });

  React.useEffect(() => {
    fetch("https://catfact.ninja/fact")
      .then((resp) => resp.json())
      .then((json) => setCatFact(json));
  }, []);

  return (
    <div>
      <Liste data={data} />
      {catfact.fact}
    </div>
  );
}

export default App;
